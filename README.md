# Verticoramapp

Équipe Le Surplus

## Description <a name="description"></a>

Nous développerons un application de gestion d'installations de culture hydroponique. Nous pensons utiliser Flutter, un kit de développement de logiciel (SDK), afin de développer notre application mobile. 

## Table des matières <a name="tabMatieres"></a>
1. [Description](#description)
2. [Table des matières](#tabMatieres)
3. [Projet](#projet)
   1. [Membres de l'équipe](#memebresEquipe)
   2. [Objectif Général](#objectifGen)
   3. [Objectif Spécifiques](#objectifSpec)
   4. [Technologies utilisées](#techUtilisees)
   5. [Contribution de chaque membre de l'équipe](#ContributionMembres)
      1. [Parite A](#ContributionA)
      2. [Partie B](#ContributionB)
      3. [Partie C](#ContributionC)
      4. [Partie D](#ContributionD)
5. [Installation](#installation)
   1. [Prérequis](#prerequis)
   2. [Installation](#installationSub)
   3. [Installation Test](#installationTest)
   4. [Dockerisation de Flutter](#dockerisationFlutter)
6. [Utilisation](#utilisation)
7. [Émulation](#emulation)
8. [Documentation](#documentation)
9. [Docummentation officiel de Flutter (anglais)](#docOfficielAnglais)
10. [Serveurs](#Serveurs)
11. [Jenkins](#Jenkins)
12. [Images de présentaion](#Images)

## Projet<a name="projet"></a>

### Membres de l'équipe <a name="memebresEquipe"></a>
 
 - Ivan Belchev
 - Carlos Pizarro
 - Daniel Rodriguez
 - Alyssa Voyzelle-Montminy

### Objectif Général <a name="objectifGen"></a>

Developper la premiere itération du mandat fait par Verticorama.

### Objectifs Spécifiques <a name="objectifSpec"></a>

Afin d'approfondir nos connaissances du développement d'interface mobile avec Flutter, nous avons sélectionné quelques objectifs secondaires :

- Creation du "Hello World". 

- Tester widget "Hello World". 

- Implémentation de Docker 

- Dockeriser l'application Flutter. 

### Technologies utilisées <a name="techUtilisees"></a>

- Android Studio - Android Studio est un environnement de développement pour développer des applications mobiles Android.

- C# - Langage de programmation orientée objet.

- Plate-forme Dart - Langage de programmation optimisé pour les applications sur plusieurs plateformes. Il est développé par Google et est utilisé pour créer des applications mobiles, de bureau, de serveur et web.

- Kotlin - Langage de programmation orienté objet et fonctionnel qui permet de compiler pour la machine virtuelle Java, JavaScript.

- Les widgets Material Design implémentent le langage de conception de Google du même nom.

- Visual Studio Code

- Flutter

### Contribution de chaque membre de l'équipe <a name="ContributionMembres"></a>

## Partie A <a name="ContributionA"></a>

- Carlos
   - Création du projet de base et création des test unitaire.
- Daniel
   - Création du document readMe et aide à la création du projet de base.
- Ivan
   - Création du repo Gitlab, aide à la création du projet de base et complétion du readMe.
- Alyssa
   - Recherche sur la documentation sur Flutter, Docker, Android Studio et Dart

## Partie B <a name="ContributionB"></a>
- Carlos
   - création du dockerfile, modification du dockerfile et aide à l'installation du projet.
- Daniel
   - création du dockerfile, modification du dockerfile et aide à l'installation du projet.
- Ivan
   - Modification du ReadMe et aide à l'installation du projet.
- Alyssa
   - Na

## Partie C <a name="ContributionC"></a>
- Carlos
   - création du fichier pubspec.yaml et recherche sur les façons de faire les fichiers docker-compose, proposition mini-serveur python pour déployer APK - lien donné par le prof.
- Daniel
   - modification du fichier pubspec.yaml et recherche sur les façons de faire les fichiers docker-compose, proposition mini-serveur python pour déployer APK - en utilisant Flask.
- Ivan
   - Modification du ReadMe,recherche sur les façons de faire les fichiers docker-compose et création du docker-compose pour le serveur de tests.
- Alyssa
   - Recherche sur les façons de faire les fichiers docker et docker-compose.

## Partie D <a name="ContributionD"></a>
- Carlos
   - recherche sur l'utilisation de jenkins et complétion de la partie C du TP.
- Daniel
   - recherche sur l'utilisation de jenkins et création du serveur jenkins.
- Ivan
   - recherche sur l'utilisation de jenkins et modification du readMe.
- Alyssa
   - recherche sur l'utilisation de jenkins et création du serveur jenkins.
## Installation <a name="installation"></a>

En premier lieu, pour installer Flutter, il faut se rendre sur ce site. https://flutter.dev/docs/get-started/install.
Par la suite, il faut choisir le systeme d'exploitation sur lequel on veut installer ce kit de développement logiciel. Il faut également installer Android Studio à partir de ce site. https://developer.android.com/studio.

### Prérequis <a name="prerequis"></a>

Bien sûr, tout dépendant le systeme d'exploitation choisi, les recommandations minimales pour les performances de l'ordinateur diffèrent. Le tout est très bien inscrit sur le site de Flutter, après avoir sélectionné sur quel system d'exploitation Flutter sera installé.

### Installation <a name="installationSub"></a>

1. Télécharger et installer le SDK de Flutter. (https://storage.googleapis.com/flutter_infra/releases/stable/windows/flutter_windows_1.22.2-stable.zip)
2. Dans les variable d'environnement de votre ordinateur, ajouter «flutter\bin» à la variable qui s'appelle «Path».
3. En ligne de commande, aller dans le dossier «.../flutter/bin» et exécuter la commande «flutter doctor». Cette commande vérifie que tout a bien été installé correctement.
4. Installer Android Studio. (https://developer.android.com/studio)

### Installation Test <a name="installationTest"></a>

Avec le SDK flutter les tests sont déjà installés. On est capable de tester les classes ainsi que les widgets, dans le cadre de notre projet on va tester les widgets, la partie visuelle avec laquelle les utilisateurs interagissent. Pour ce faire il faudra:

1. Ajouter les dependences dans le fichier pubspec.yaml (dev_dependencies: flutter_test: sdk: flutter)
2. Créer la classe test qui va tester le Widget.
3. Pour faire rouler le test click droite sur le fichier test et 'Run test ...'
4. Le résultat du test va apparaitre dans la fenêtre Run du AndroidStudio.

Des informations plus détaillées ici dans la page dev de Flutter : https://flutter.dev/docs/cookbook/testing/widget/introduction

### Dockerisation de Fluter <a name="dockerisationFlutter"></a>

Nous avons utiliser ce site comme référence pour savoir comment utiliser docker avec la technologie Flutter. https://medium.com/flutter-community/how-to-dockerize-flutter-apps-f2e54d6ec43c.

1. Installer Docker sur l'ordinateur.
2. Mettre en place Visual Studio Code sur la machine et installer les extensions Docker et Remote Developpement.
3. Créer le conteneur Docker avec tous les fichiers et dossiers nécessaires.
4. construire et faire marcher le conteneur Docker.

## Utilisation <a name="utilisation"></a>

Avant tout il faudra partir un projet Flutter à partir de la fenêtre de départ d'Android (Ou VsCode).
- Sélectionner Nouveau projet Flutter.
- Sélectionner Flutter application et Cliquer sur Next.
- Dans la section flutter SDK path il faudra mettre l'emplacement exact du dossier contenant la SDK Flutter (C'est très important, 
  sinon le projet ne pourra pas trouver les SDK nécessaire).
- Cliquer sur finish et vos etes pret à programmer sur Flutter.

À partir de ce moment vous pourriez cloner tous les projets Flutter et l'emplacement des SDK nécessaires seront déjà configurés 
sur votre environnement de programmation.

## Émulation <a name="emulation"></a>

Il y a trois façons de pouvoir partir votre projet:
1) À partir de l'émulateur préalablement installé pour l'environnement de travail.
2) Brancher un cellulaire Android sur votre ordinateur, et faire partir le projet de façon régulière sur l'environnement de travail.
3) Sur ligne de commande, placez-vous dans le dossier où le projet se trouve et le faire rouler avec la ligne de commande flutter Run. (Soit sur l'Émulateur ou votre cellulaire branché).

## Documentation <a name="documentation"></a>

Voici la procedure (en anglais) officiel pour deployer Flutter pour une première fois.

Notez qu'il est preferable d'installer Flutter avant d'installer Android Studio.
https://flutter.dev/docs/get-started/install/windows

Vous pouvez aussi utiliser d'autres logiciels d'edition comme VS Code.
https://flutter.dev/docs/development/tools/vs-code

Vous pouvez utiliser ce glossaire contenant des termes de soccer.
https://www.hometeamsonline.com/teams/popups/Glossary.asp?s=soccer&t=print 

Documentation sur Dart.
https://dart.dev/guides



### Docummentation officiel de Flutter (anglais) <a name="docOfficielAnglais"></a>

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Serveurs<a name="Serveurs"></a>

Dans notre projet, nous avons 1 branche pour chaques serveur de chaques étapes du projet. 
Nous avons un serveur de production, de développement, de staging et de test. 
De plus, nous mettons en place un petit serveur en python pour pouvoir faire rouler notre projet dans toutes les étapes du développement. 
Nous avons un fichier docker-compose pour les 4 serveurs mais nous n'utilisons qu'une partie du fichier docker-compose pour chacun des serveurs de développement.

### Jenkins<a name="Jenkins"></a>

Pour utiliser Jenkins, il faut d'abord avoir installé docker et docker daemon.
Il faut par la suite, installer Jenkins et le faire rouelr sur la machine.
Il faut ensuite ajouter des lignes de code dans le fichier dockerfile pour démarrer l'environnement de travail Jenkins avec notre projet de dévelopement.
Il faut aussi créer 4 jobs pour les 4 étapes de notre environement de travail.
De plus, il faut faire en sorte qu'il y ait de la persistence entre le Jenkins et notre container Docker.
Donc, il faut aller dans le tableau de bord -> Dev -> Source Code Management pour créer un lien entre le le repository Gitlab et l'environnement Jeankins.

### Images de présentaion <a name="Images"></a>
<img src="img/classTest.png">
<img src="img/dossierTest.png">
<img src="img/Screenshot_20201113-142320.jpg" height="500"/>
<img src="img/test.png">
<img src="img/test2.png">
<img src="img/testReussi.png">
<img src="img/Docker_plugin.png">
